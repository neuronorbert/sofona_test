$( document ).ready(function() {
  // Animate-js scripts
  $('.is-fadeInDown').animated('fadeInDown', 'fadeOutUp');
  $('.is-fadeInUp').animated('fadeInUp', 'fadeOutDown');
  $('.is-fadeInRight').animated('fadeInRight', 'fadeOutLeft');
  $('.is-fadeInLeft').animated('fadeInLeft', 'fadeOutRight');
});